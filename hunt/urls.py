from django.urls import path, include

from . import views

app_name = 'hunt'
urlpatterns = [
    path('', views.index,  name='index'),
    path('user/<int:pk>', views.TeamView.as_view(), name='user'),
    path('clue/<int:pk>', views.ClueView.as_view(), name='clue'),
    path('decrypt/', views.decrypt, name='decrypt'),
    path('register', views.register, name="register"),
    path('scan',views.scan, name="scan"),
    path('rules',views.rules, name="rules"),
    path('prizes',views.prizes, name="prizes"),
    path('leaderboard',views.leaderboard, name="leaderboard"),
]

urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls'))
]