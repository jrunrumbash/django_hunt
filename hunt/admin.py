from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from import_export import resources

from .models import Clue

class ClueResource(resources.ModelResource):
    class Meta: 
        model = Clue

class ClueResourceAdmin(ImportExportModelAdmin):
    fields = ['name', 'location_hint', 'code', 'treasure_icon']
    list_display = ['id','name', 'hash', 'location_hint']
    resource_class = ClueResource

admin.site.register(Clue, ClueResourceAdmin)
