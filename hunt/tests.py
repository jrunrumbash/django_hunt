from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from django_webtest import WebTest
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.test.client import Client

from .models import Clue, ClueStatus
from .forms import LoginForm
from hashlib import sha224

# Create your tests here.
TEST_USERNAME = 'TestUser'
TEST_PASSWORD = 'T3stPa55word'

def createClue(name, location_hint, code='test_code'):
    return Clue.objects.create(name=name, location_hint=location_hint, code=code)


def createUser(name):
    username = name
    password = TEST_PASSWORD
    return User.objects.create(username=username, email=username+'@ericsson.com', password=password)

def createTestUser():
    return createUser(TEST_USERNAME)

class UserViewTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.clue = createClue('testClue', 'I am here')
        self.user = createTestUser()
        self.client.force_login(self.user)

    def test_add_clue_to_team(self):
        response = self.client.get(reverse('hunt:user', args=(self.user.id,)))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, 'Unknown Treasure')

    def test_solve_clue_shows_name(self):
        ClueStatus.objects.update(user=self.user, clue=self.clue, solved=True)
        response = self.client.get(reverse('hunt:user', args=(self.user.id,)))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.clue.id)

class LoginViewTest(WebTest):

    def setUp(self):
        createTestUser()
        

    def test_login_submission_success(self):
        page = self.app.get('/hunt/accounts/login/')
        page.form['username'] = TEST_USERNAME
        page.form['password'] = TEST_PASSWORD
        page = page.form.submit()
        self.assertContains(page, '/hunt')
        self.assertEquals(page.status_code, 200)

    def test_login_submission_failure(self):
        page = self.app.get('/hunt/accounts/login/')
        page.form['username'] = TEST_USERNAME
        page.form['password'] = 'incorrectpassword'
        page = page.form.submit()
        self.assertContains(page, reverse('hunt:login',))
        self.assertEquals(page.status_code, 200)

class RegistrationViewTest(WebTest):

    def test_valid_registration(self):
        form=self.app.get('/hunt/register').form
        form['username'] = TEST_USERNAME
        form['password1'] = TEST_PASSWORD
        form['password2'] = TEST_PASSWORD
        team_page = form.submit() 
        user = authenticate(username = TEST_USERNAME, password = TEST_PASSWORD)
        self.assertRedirects(team_page, reverse('hunt:user', args=(user.id,)))

class DecryptionTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.clue = createClue('testClue', 'This is a location hint')
        self.user = createTestUser()
        self.client.force_login(self.user)

    def test_decryption_for_qr_get(self):
        hash = self.clue.hash
        clue_id = self.clue.id
        url = '/hunt/decrypt/?decrypt_code={}&clue_id={:d}'.format(hash, clue_id)
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, self.clue.name)

class LeaderBoardTest(TestCase):

    def testAllPlayersAreDisplayed(self):
        user1 = createUser('player1')
        user2 = createUser('player2')
        user3 = createUser('player3')
        response = self.client.get(reverse('hunt:leaderboard'))
        self.assertEquals(response.status_code, 200)
        self.assertContains(response, user1.username)
        self.assertContains(response, user2.username)
        self.assertContains(response, user3.username)
        # Should not display the admin
        self.assertNotContains(response, 'admin')

    def testGetTotalClues(self):
        createClue('clueA','locationA', 'clueA')
        createClue('clueB','locationB', 'clueB')
        createClue('clueC','locationC', 'clueC')
        user = createTestUser()
        total = ClueStatus.get_total(user)
        score = ClueStatus.get_score(user)
        self.assertEquals(total, 3, 'Total does not match number of clues')
        self.assertEquals(score, 0)

    def testDisplaysChangeOnCompletion(self):
        clue = createClue('clueA','locationA', 'codeA')
        user = createTestUser()
        clue_status = ClueStatus.objects.filter(user=user, clue=clue)[0:1].get()
        clue_status.solved = True
        clue_status.save()
        self.assertEquals(ClueStatus.get_total(user), 1)
        self.assertEquals(ClueStatus.get_score(user), 1)
        response = self.client.get(reverse('hunt:leaderboard'))
        self.assertContains(response, 'completion_image')
