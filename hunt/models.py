from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from hashlib import sha224

# Create your models here.

class Clue(models.Model):
    name = models.CharField(max_length=200, unique=True)
    hash = models.CharField(max_length=200, editable=False)
    location_hint = models.CharField(max_length=200)
    treasure_icon = models.ImageField(null=True, default='images/chest.png')
    code = models.CharField(max_length=200, unique=True, default='code')

    def save(self, **kwargs):
        if not self.id:
            name = self.name.encode('utf-8')
            self.hash = sha224(name).hexdigest()
        super(Clue, self).save() # Call the real save() method

class Image(models.Model):
    clue = models.ForeignKey(Clue, on_delete=models.CASCADE)
    link = models.CharField(max_length=200)


class ClueStatus(models.Model):
    clue = models.ForeignKey(Clue, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    solved = models.BooleanField()

    def get_score(user):
        return ClueStatus.objects.filter(user=user).filter(solved=True).count()

    def get_total(user):
        return ClueStatus.objects.filter(user=user).count()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        print('Creating user: ' + instance.username)
        clues = Clue.objects.all()
        for clue in clues:
            clueState = ClueStatus.objects.create(user=instance, clue=clue, solved=False)
