from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User
from django.views import generic
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import Clue, ClueStatus
from .forms import LoginForm


class TeamView(LoginRequiredMixin, generic.DetailView):
    model = User
    template_name = 'hunt/team.html'

class ClueView(LoginRequiredMixin, generic.DetailView):
    model = Clue
    template_name = 'hunt/clue.html'

def index(request):
    welcome_message = 'Welcome to the hunt. Hidden around the building are the markers \r that will lead to the treasure. \r Good Luck!'
    return render(request, 'hunt/index.html', {'welcome_message' : welcome_message}, )

@login_required(login_url='/hunt')
def decrypt(request):
    user = request.user
    if request.method == 'POST':
        try:
            clue_id = request.POST.get('clue_id')
            decrypt_code = request.POST.get( 'decrypt_code' )
            selected_clue_set = ClueStatus.objects.filter(user__username=user.username, clue__id__exact=clue_id)[0:1].get()
        except(KeyError, ClueStatus.DoesNotExist):
            return render(request, 'hunt/team.html', {
                'error_message': 'Failed to find a matching clue!',
            })
    elif request.method == 'GET':
        clue_id = request.GET.get('clue_id')
        decrypt_code = request.GET.get( 'decrypt_code' )
        selected_clue_set = ClueStatus.objects.filter(user__username=user.username, clue__id__exact=clue_id)[0:1].get()

    clue = selected_clue_set.clue
    if clue.hash in decrypt_code:
        selected_clue_set.solved = True
        selected_clue_set.save()
        return render(request, 'hunt/team.html')

    return render(request, 'hunt/team.html', {
        'error_message': 'Failed to decrypt! Unlock code does not match!',
    })

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect(reverse('hunt:user', args=(user.id,)))
    else:
        form = UserCreationForm()
    return render(request, 'hunt/register.html', {'form': form,})

def leaderboard(request):
    player_users = User.objects.exclude(username__exact='root')
    table = []
    for player in player_users:
        score = ClueStatus.get_score(user=player)
        total = ClueStatus.get_total(user=player)
        table.append({'player':player.username, 'score':score, 'total':total})
    return_table = sorted(table, key=lambda k: k['score'])
    return render(request, 'hunt/leaderboard.html', {'table': return_table,})

def rules(request):
    return render(request, 'hunt/rules.html')

def scan(request):
    return render(request, 'hunt/scan.html')

def prizes(request):
    return render(request, 'hunt/prizes.html')